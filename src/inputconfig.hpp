/**
 * @file inputmanager.hpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @date 2020-12-10
 *
 * @copyright Copyright © 2020. This file is under the CC by-nc-sa 4.0 license
 * Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International.
 * The full text of this license may be found here:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 */

#ifndef INPUTS_HPP_
#define INPUTS_HPP_

#include <math.h>

#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <variant>
#include <vector>

#include "CLI11.hpp"

class inputconfig {
 private:
  static inputconfig *instance; ///< instance of the input configurator
  ~inputconfig() {}
  inputconfig(const inputconfig &); ///< Copy constructor
  inputconfig &operator=(const inputconfig &); ///< Operator "equal" = 

 public:
  CLI::App *app; ///< CLI app used by the program
  /**
   * @brief Get the Instance of inputconfig
   */
  static inputconfig *getInstance() {
    if (instance == NULL) {
      instance = new inputconfig();
    }
    return instance;
  }

 protected:
 /**
  * @brief Construct a new inputconfig object
  * 
  */
  inputconfig() {
    std::cerr << "gconfig created\n";
    app = new CLI::App("Quaternion");
    app->set_config("--config");
  }
};

#endif  // INPUTS_HPP_
