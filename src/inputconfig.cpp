/**
 * @brief Creates the instance of the factory. This is needed in c++ when using the static factory pattern.
 * @file inputmanager.cpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @date 2020-12-10
 *
 * @copyright Copyright © 2020. This file is under the CC by-nc-sa 4.0 license
 * Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International.
 * The full text of this license may be found here:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 */

#include "inputconfig.hpp"

inputconfig *inputconfig::instance = NULL;