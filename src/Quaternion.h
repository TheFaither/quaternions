/**
 * @file Quaternion.h
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @version 0.1
 * @date 2021-10-22
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef QUATERNION_H
#define QUATERNION_H

#define EIGEN_DENSEBASE_PLUGIN "EigenQuaternionPlugin.hpp"

#include <eigen3/Eigen/Eigen>
#include <vector>
#include <iostream>
#include <complex>

typedef Eigen::Matrix4d mat4; ///< 4x4 Eigen matrix
typedef Eigen::Vector4d vec4; ///< 4x1 Eigen Vector
typedef Eigen::Vector3d vec3; ///< 3x1 Eigen Vector
typedef std::vector<double> stdvec; ///< std::vector of doubles. Eigen::VectorXd can be constructed by Eigen::VectorXd(std::vector<double>.data())

/**
 * @brief Class describing a Hamilton quaternion \f$a+b\vec{i}+c\vec{j}+d\vec{k}\f$
 *
 */
class Quaternion
{
private:
    vec4 qdata; ///< vector containing the 4 values a, b, c, d forming the quaternion $a + bi + cj + dk$

public:
    /* ------------------------------ Constructors ------------------------------ */

    Quaternion();
    Quaternion(double a, double b, double c, double d);
    Quaternion(vec3 axis, double theta);
    Quaternion(stdvec iqdata);
    Quaternion(vec4 iqdata);
    ~Quaternion();

    /* --------------------------------- Getters -------------------------------- */
    vec4 getdata() const;                            ///< Eigen::Vector4d of coefficients
    inline double a() const { return qdata.a(); }    ///< real part
    inline double b() const { return qdata.b(); }    ///< first immaginary coefficient
    inline double c() const { return qdata.c(); }    ///< second immaginary coefficient
    inline double d() const { return qdata.d(); }    ///< third immaginary coefficient
    inline double real() const { return qdata.a(); } ///< real part
    vec3 complexpart() const;

    /* --------------------------------- Fillers -------------------------------- */
    inline void identity() { qdata << 1, 0, 0, 0; }                             ///< set Quaternion to the identity quaternion \f$\left( 1,0,0,0\right)\f$
    inline void null() { qdata << 0, 0, 0, 0; }                                 ///< set Quaternion to the null quaternion \f$\left( 0,0,0,0\right)\f$
    inline Quaternion conjugate() { return Quaternion(a(), -b(), -c(), -d()); } ///< returns conjugate Quaternion \f$\left( a,-b,-c,-d\right)\f$

    /* -------------------------------------------------------------------------- */
    /*                                   Casters                                  */
    /* -------------------------------------------------------------------------- */
    /** @brief Cast Quaternion to std::vector<double> */
    operator stdvec() const { return stdvec{a(), b(), c(), d()}; }

    /** @brief Cast Quaternion to Eigen::Vector4d*/
    operator vec4() const { return getdata(); }

    /* -------------------------------------------------------------------------- */
    /*                                  Operators                                 */
    /* -------------------------------------------------------------------------- */
    /* -------------------------------- Negative -------------------------------- */
    Quaternion operator-() const;
    /* ----------------------------------- Sum ---------------------------------- */
    Quaternion operator+(const Quaternion &q1);
    Quaternion operator-(const Quaternion &q1);
    /* ----------------------------- Multiplication ----------------------------- */
    Quaternion operator*(const double &l);
    Quaternion operator/(const double &l);
    Quaternion operator*(const Quaternion &q);
    /* -------------------------------------------------------------------------- */
    /*                                    Math                                    */
    /* -------------------------------------------------------------------------- */
    /* ---------------------------------- Norm ---------------------------------- */
    /** @brief Return 2-norm \f$\left|q\right| = \sqrt{q\cdot q^*}\f$ of the quaternion */
    inline double norm()
    {
        return sqrt(a() * a() +
                    b() * b() +
                    c() * c() +
                    d() * d());
    }
    /* --------------------------------- Inverse -------------------------------- */
    /** @brief Return inverse Quaternion \f$ q^{-1} = \frac{q^*}{\left|q\right|} \f$ */
    inline Quaternion inverse()
    {
        return conjugate() / norm();
    }

    /* -------------------------------- Distance -------------------------------- */
    /** @brief Return the distance between the Quaternion and another Quaternion \f$p\f$,
     *  given by \f$d \left(q,p \right) = \left|q-p \right| \f$
     * */
    inline double const distance(const Quaternion p)
    {
        return (*this - p).norm();
    }

    /* -------------------------------------------------------------------------- */
    /*                                 Matrix Form                                */
    /* -------------------------------------------------------------------------- */
    Eigen::Matrix2cd const matrixform();
};
/* -------------------------------------------------------------------------- */
/*                                   Rotate                                   */
/* -------------------------------------------------------------------------- */
vec3 const rotateVector(const vec3 v, const Quaternion q);

/* -------------------------------------------------------------------------- */
/*                                  Distance                                  */
/* -------------------------------------------------------------------------- */
/** @brief Return the distance between two Quaternion \f$q\f$ and \f$p\f$,
 *  given by \f$d \left(q,p \right) = \left|q-p \right|
 * */
inline double const distance(Quaternion q, const Quaternion p)
{
    return (q - p).norm();
}

/* -------------------------------------------------------------------------- */
/*                                   Stream                                   */
/* -------------------------------------------------------------------------- */
std::ostream &
operator<<(std::ostream &stream, Quaternion q);

#endif // QUATERNION_h