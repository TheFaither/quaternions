/**
 * @file Quaternion.cpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @version 0.1
 * @date 2021-10-22
 *
 * @copyright Copyright (c) 2021
 *
 */


#include "Quaternion.h"

/**
 * @brief Construct a new Quaternion:: Quaternion object with \f$a=b=c=d=0\f$
 * 
 */
Quaternion::Quaternion()
{
  qdata << 0, 0, 0, 0;
}

/**
 * @brief Construct a new Quaternion:: Quaternion object using 4 double values
 * 
 * @param a real parameter
 * @param b first immaginary parameter
 * @param c second immaginary parameter
 * @param d third immaginary parameter
 */
Quaternion::Quaternion(double a, double b, double c, double d)
{
  qdata << a, b, c, d;
}

/**
 * @brief Construct a new Quaternion:: Quaternion object from one axis of rotation and an angle of rotation
 * 
 * @param u axis of rotation
 * @param th angle of rotation
 */
Quaternion::Quaternion(vec3 u, double th)
{
  qdata << cos(th / 2), sin(th / 2) * u[0], sin(th / 2) * u[1], sin(th / 2) * u[2];
}

/**
 * @brief Construct a new Quaternion:: Quaternion object from a std::vector<double of length 4
 * 
 * @param i vector containing the 4 value a, b, c, d with \f$a + bi + cj + dk\f$
 */
Quaternion::Quaternion(stdvec i)
{
  qdata << i[0], i[1], i[2], i[3];
}

/**
 * @brief Construct a new Quaternion:: Quaternion object from a Eigen::Vector4d
 * 
 * @param i 
 */
Quaternion::Quaternion(vec4 i)
{
  qdata = i;
}

Quaternion::~Quaternion()
{
}

/* -------------------------------------------------------------------------- */
/*                                   Getters                                  */
/* -------------------------------------------------------------------------- */
vec4 Quaternion::getdata() const
{
  return qdata;
}

/**
 * @brief Returns the Eigen::Vector3d containing the coefficients of the complex part of the Quaternion \f$b\vec{i}+c\vec{i}+d\vec{k}\f$
 * 
 */
vec3 Quaternion::complexpart() const
{
  return vec3{b(),c(),d()};
}

/* -------------------------------------------------------------------------- */
/*                                  Operators                                 */
/* -------------------------------------------------------------------------- */
/* -------------------------------- Negative -------------------------------- */
/**
 * @brief Unary operation returning the negative value of the Quaternion
 * 
 */
Quaternion Quaternion::operator-() const
{
  return Quaternion(-a(), -b(), -c(), -d());
}

/* ----------------------------------- Sum ---------------------------------- */
/**
 * @brief Binary operation of sum between two quaternions
 * 
 */
Quaternion Quaternion::operator+(const Quaternion &q1)
{
  return Quaternion(qdata + q1.getdata());
}
/**
 * @brief Binary operation of subtraction between two quaternions
 * 
 */
Quaternion Quaternion::operator-(const Quaternion &q1)
{
  return Quaternion(qdata + (-q1.getdata()));
}

/* -------------------------- Scalar Multiplication ------------------------- */
/**
 * @brief Binary operation of multiplication between a Quaternion and a double
 */
Quaternion Quaternion::operator*(const double &l)
{
  return Quaternion(qdata * l);
}
/**
 * @brief Binary operation of division between a Quaternion and a double
 * 
 */
Quaternion Quaternion::operator/(const double &l)
{
  return Quaternion(qdata / l);
}
/* ------------------------ Quaternion Multiplication ----------------------- */
/**
 * @brief Binary operation of multiplication between two Quaternions
 */
Quaternion Quaternion::operator*(const Quaternion &q)
{
  vec4 res;
  res << a() * q.a() - b() * q.b() - c() * q.c() - d() * q.d(),
      a() * q.b() + b() * q.a() + c() * q.d() - d() * q.c(),
      a() * q.c() - b() * q.d() + c() * q.a() + d() * q.b(),
      a() * q.d() + b() * q.c() - c() * q.b() + d() * q.a();
  return Quaternion(res);
}

/* -------------------------------------------------------------------------- */
/*                                 Matrix Form                                */
/* -------------------------------------------------------------------------- */

/**
 * @brief Returns the complex matrix form of the Quaternion
 * */
Eigen::Matrix2cd const Quaternion::matrixform()
{
  using namespace std::literals::complex_literals;
  Eigen::Matrix2cd res;
  res << a() + b() * 1i,
      +c() + d() * 1i,
      -c() + d() * 1i,
      +a() - b() * 1i;
  return res;
}



/* -------------------------------------------------------------------------- */
/*                                   Rotate                                   */
/* -------------------------------------------------------------------------- */
/**
 * @brief Given a vector \f$v\f$ and a quaternion \f$q\f$, it returns the rotated vector \f$v_1 = q \cdot q_v \cdot q^*\f$, where \f$q_v\f$ is a quaternion of null real part and complex part equal to \f$v\f$
 * 
 * @param v vector to be rotated
 * @param q rotating quaternion
 * @return vec3 rotated vector
 */
vec3 const rotateVector(vec3 v, Quaternion q)
{
  Quaternion qv(0, v[0], v[1], v[2]);
  return (q * qv * q.inverse()).complexpart();
}

/* -------------------------------------------------------------------------- */
/*                                   Stream                                   */
/* -------------------------------------------------------------------------- */
/**
 * @brief ostream operator for Quaternion
 */
std::ostream &operator<<(std::ostream &stream, Quaternion q)
{
  stream << q.getdata().a() << ", " << q.getdata().b() << ", " << q.getdata().c()<< ", "  << q.getdata().d() << std::endl;
  return stream;
}
