/**
 * @file test.cpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @version 0.1
 * @date 2021-10-22
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "Quaternion.h"
#include "inputconfig.hpp"

int main(int argc, char const *argv[])
{
    /* ------------------------- Variables needed by CLI ------------------------ */
    stdvec dq, dp;
    stdvec rotaxis, rotvec;
    double rottheta;
    Quaternion q, p;
    /* ----------------- Get instance of the input configurator ----------------- */
    inputconfig *gconf = inputconfig::getInstance();

    /* -------------------------------------------------------------------------- */
    /*                                     CLI                                    */
    /* -------------------------------------------------------------------------- */
    /* ----------------------------- Test subcommand ---------------------------- */
    CLI::App *subtest = gconf->app->add_subcommand("test",
                                                   "Run a test to check that quaternion multiplication is properly executed on the machine");
    /* -------------------------- Operation subcommand -------------------------- */
    CLI::App *suboperations = gconf->app->add_subcommand("op",
                                                         "Runs simple operations (check subcommand help for more information)");
    auto optq = suboperations->add_option("-q,--q,--q1", dq, "a b c d of first quaternion (separator = ' ')")
                    ->ignore_case()
                    ->expected(4);
    auto optp = suboperations->add_option("-p,--p,--q2", dp, "a b c d of second quaternion (separator = ' ')")
                    ->ignore_case()
                    ->expected(4);
    auto subconj = suboperations->add_subcommand("conjugate", "Print the conjugate of the first quaternion")
                       ->needs(optq);
    auto subinv = suboperations->add_subcommand("inverse", "Print the inverse of the first quaternion")
                      ->needs(optq);
    auto subsum = suboperations->add_subcommand("sum", "Print the sum of the two quaternions")
                      ->needs(optq)
                      ->needs(optp);
    auto submul = suboperations->add_subcommand("multiply", "Print the two multiplications of the two quaternions")
                      ->needs(optq)
                      ->needs(optp);
    /* --------------------------- Rotation subcommand -------------------------- */
    CLI::App *subrotation = gconf->app->add_subcommand("rotation",
                                                       "Rotate a vector by a given angle around a given axis");
    subrotation->add_option("--vector", rotvec, "x y z of vector to be rotated")
        ->ignore_case()
        ->expected(3)
        ->required();
    subrotation->add_option("--theta", rottheta, "angle of rotation")
        ->ignore_case()
        ->required();
    subrotation->add_option("--axis", rotaxis, "x y z of axis of rotation")
        ->ignore_case()
        ->expected(3)
        ->required();
    /* -------------------------------- Parse CLI ------------------------------- */
    CLI11_PARSE(*gconf->app, argc, argv);

    /* -------------------------------------------------------------------------- */
    /*                                 Operations                                 */
    /* -------------------------------------------------------------------------- */
    if (suboperations->parsed())
    {
        /* ------------------------------------ q ----------------------------------- */
        q = {dq};
        std::cout << "q: \n"
                  << q << std::endl;
        /* ------------------------------ p (if needed) ----------------------------- */
        if (optp->count() > 0)
        {
            std::cout << "p: \n"
                      << p << std::endl;
            p = {dp};
        }

        /* --------------------------------- Inverse -------------------------------- */
        if (subinv->parsed())
        {
            std::cout << "q^-1 = \n"
                      << q.inverse() << std::endl;
        }
        /* -------------------------------- Conjugate ------------------------------- */
        if (subconj->parsed())
        {
            std::cout << "q* = \n"
                      << q.conjugate() << std::endl;
        }
        /* ----------------------------------- Sum ---------------------------------- */
        if (subsum->parsed())
        {
            std::cout << "q+p = \n"
                      << q + p << std::endl;
        }

        /* ----------------------------- Multiplication ----------------------------- */
        if (submul->parsed())
        {
            // I was unable to determine which quaternion was called first
            // I thus execute both multiplications
            // I can further look into it if necessary
            std::cout << "q*p = \n"
                      << q * p << std::endl;
            std::cout << "p*q = \n"
                      << p * q << std::endl;
        }
    }

    /* -------------------------------------------------------------------------- */
    /*                                  Rotation                                  */
    /* -------------------------------------------------------------------------- */
    if (subrotation->parsed())
    {
        std::cout << "Rotation\n";
        vec3 vaxis(rotaxis.data());
        vec3 vrotvec(rotvec.data());
        Quaternion r(vaxis, -rottheta);
        std::cout << "axis:\n"
                  << vaxis << ";\ntheta:\n"
                  << rottheta << std::endl;
        std::cout << "Rotation of\n"
                  << vrotvec << "\nis\n"
                  << rotateVector(vrotvec, r);
    }

    /* -------------------------------------------------------------------------- */
    /*                                    Test                                    */
    /* -------------------------------------------------------------------------- */
    if (subtest->parsed())
    {
        std::cout << "Test\n";
        vec4 dq, dp;
        dq << 1, 2, 3, 4;
        dp << -0.5, 0.5, -0.5, 0.5;
        Quaternion q{dq}, p{dp};
        Quaternion r = q * p;
        std::cout << "q = \n"
                  << q << "\np = \n"
                  << p << "\nexpected value of q*p = -2,3,-1,-4\n"
                  << "\nq*p = \n"
                  << r << std::endl;
        // Fails if q*p is not the expected value
        if (r.getdata() != vec4(-2, 3, -1, -4))
            return 1;
    }

    return 0;
}
