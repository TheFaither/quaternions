/**
 * @file test.cpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @version 0.1
 * @date 2021-10-22
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "Quaternion.h"
#include "inputconfig.hpp"

int main(int argc, char const *argv[])
{
    /* ------------------------- Variables needed by CLI ------------------------ */
    stdvec rotaxis, rotvec;
    double rottheta;

    std::cout << "Test\n";
    vec4 vq, vp;
    vq << 1, 2, 3, 4;
    vp << -0.5, 0.5, -0.5, 0.5;
    Quaternion q{vq}, p{vp};

    std::cout << "q = \n"
              << q << "\np = \n"
              << p << std::endl;

    std::cout << "real part of q: " << q.real() << std::endl;
    std::cout << "complex part of q: " << q.complexpart() << std::endl;

    std::cout << "q-p: " << q - p << std::endl;
    std::cout << "q*2: " << q * 2 << std::endl;

    std::cout << "Conjugate of q: " << q.conjugate() << std::endl;
    std::cout << "Norm of q: " << q.norm() << std::endl;
    std::cout << "Inverse of q: " << q.inverse() << std::endl;

    std::cout << "Matrix form of q: " << q.matrixform() << std::endl;
    std::cout << "Distance between q and p: " << distance(q, p) << std::endl;

    Quaternion r = q * p;
    std::cout << "\nexpected value of q*p = -2,3,-1,-4"
              << "\nq*p = " << r << std::endl;
    // Fails if q*p is not the expected value
    if (r.getdata() != vec4(-2, 3, -1, -4))
        return 1;
    else
        std::cout << "Test succeded\n";
    return 0;
}
