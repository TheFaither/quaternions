# Quaternions

A(nother) class for Quaternions in Eigen, developed for the Scientific Programmig course held by Prof. Enrico Bertolazzi at the Department of Industrial Engineering of Università degli Studi di Trento.

[Gitlab repository](https://gitlab.com/TheFaither/quaternions)

[Documentation](https://thefaither.gitlab.io/quaternions)

## Usage
The program accepts the following command-line arguments:
* `test` runs a test routine.
* `op` allows to execute inverse, conjugate, sum and multiplication operations.
* `rotation` rotates a vector.

A `Test.out` executable is provided to see an example of all the possible operations.
CTests are implemented to verify the code.

Check the helper `./build/Quaternions.out --help` for more information on how to use the executable.

## Quaternions
Quaternions are an extension of complex numbers. Quaternions are also an efficient way of representing a rotation in the 3d space using only 4 float values. 

## CLI Parser
The main function implements a Command-Line Interface (CLI). This interface utilize the C++ header-only library [CLI11](https://github.com/CLIUtils/CLI11). The CLI11 library works by providing an object called CLI::app. To prevent the creation of multiple CLI::app objects, in this code the CLI::app object is included in a class called inputconfig. A static instance of this class is initialized at runtime. 
One powerful application of CLI11 is the possibility of knowing which subcommands, options and flags were called in a human-readable one-line fashion. Here we show how CLI::app->parsed() can be used to execute some parts of the code only if the corresponding app has been called by the user, but CLI also provides the possibility of writing specific callbacks for each flag, option and command, avoiding unnecessary if nesting (as the CLI11 documentation says, "procedural, yuck").

## Gitlab CI
A simple CI is implemented in this project. The CI uses the [official gcc docker image](https://hub.docker.com/_/gcc).
### Static analysys
The CI runs two static analysis of the C++ code using [cpplint](https://github.com/cpplint/cpplint) and [cppcheck]. No warnings evoked by the static analysis can lead to a broken pipeline.
### Documentation
The CI compiles the doxygen documentation and uploads it to the corresponding [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) website, found [here](https://thefaither.gitlab.io/quaternions/). A failure in the documentation build leads to a broken pipeline.
### Build
The pipeline tries to build the code using [CMake](https://cmake.org/) and [Ninja](https://ninja-build.org/)