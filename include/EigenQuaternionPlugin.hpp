/**
 * @brief Extends the Eigen::Dense class to support functions .a(), .b(), c(), .d() 
 * returning the coefficients of the quaternion
 * @file EigenQuaternionPlugin.hpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @date 2020-06-04
 *
 * @copyright Copyright © 2020. This file is under the CC by-nc-sa 4.0 license
 * Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International.
 * The full text of this license may be found here:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 */

inline Scalar &a() { return this->operator()(0); }
inline Scalar &b() { return this->operator()(1); }
inline Scalar &c() { return this->operator()(2); }
inline Scalar &d() { return this->operator()(3); }
inline Scalar a() const { return this->operator()(0); }
inline Scalar b() const { return this->operator()(1); }
inline Scalar c() const { return this->operator()(2); }
inline Scalar d() const { return this->operator()(3); }

inline long unsigned int usize() const { return static_cast<long unsigned int>(this->size()); }

inline Scalar moduleComplexPart() const
{
  return sqrt(this->operator()(1) * this->operator()(1) +
              this->operator()(2) * this->operator()(2) +
              this->operator()(3) * this->operator()(3));
}

inline Scalar module() const
{
  return sqrt(this->operator()(0) * this->operator()(0) +
              this->operator()(1) * this->operator()(1) +
              this->operator()(2) * this->operator()(2) +
              this->operator()(3) * this->operator()(3));
}